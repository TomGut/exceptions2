/*
poprzez extend Exception dostajemy się do konstruktora
z informacja text o wyjątku
wykorzystamy go w innych klasach do rzucania wyjątków customowych
 */

public class WrongIndexException extends Exception {

    //wykorzystujemy konstruktor ze stringiem znadklasy Exception
    public WrongIndexException(String message){
        super(message);
    }


}
