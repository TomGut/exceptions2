public class ArrayOfInts {

    //tworzymy nullową tablicę
    private int[] tab;

    /*
    ten zapis int... umożliwia podanie dowolnej ilości zmiennych bez konstruowania
    następnych konstruktorów wartości te są przechowywane w zmiennej values
     */

    public ArrayOfInts(int... values){
        tab = values;
    }

    //dodajemy customowy wyjątek
    public int get(int idx) throws WrongIndexException {
        //spr czy podana wartość idx mieści się w długości tablicy
        if(isOutsideBounds(idx)){
            String message = "Zły index tablicy, podałeś " + idx + " a tablica ma " + tab.length + " indeksów";
            throw new WrongIndexException(message);
        }else{
            return tab[idx];
        }
    }

    //zrobiliśmy refaktoring kodu do metody - powtarzał się warunek
    private boolean isOutsideBounds(int idx) {
        return idx < 0 || idx >= tab.length;
    }

    public void set(int idx, int value) throws WrongIndexException {

        if(isOutsideBounds(idx)){
            String message = "Zły index tablicy, podałeś " + idx + " a tablica ma " + tab.length + " indeksów";
            throw new WrongIndexException(message);
        }else{
            tab[idx] = value;
        }
    }

    public void printAll(){
        for(int i=0; i<tab.length; i++){
            System.out.println(tab[i]);
        }
    }

}


